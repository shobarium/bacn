FROM python:3.6

RUN apt-get update && apt-get upgrade -y && apt-get autoremove && apt-get autoclean
ARG PROJECT=bacn
ARG PROJECT_DIR=/var/www/${PROJECT}

RUN pip install pipenv
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR
COPY Pipfile ./
RUN pipenv install

# Server
EXPOSE 8000
STOPSIGNAL SIGINT

#ENTRYPOINT ["pipenv", "run", "python", "manage.py"]
#CMD ["runserver", "0.0.0.0:8000"]

ENTRYPOINT ["./start"]
