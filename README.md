# Bacn

Bacn is an open source, decentralized social platform for blogging.  Currently the project is in the planning stage.

## Communications and contributing:
* IRC channel on freenode: #awesome-bacon
* Slack workspace: [bacn-social.slack.com](https://join.slack.com/t/bacn-social/shared_invite/enQtNDk4NzUwNjg0OTkyLTJmY2RlMGQ1MWExODg4ZDkzODc2NWFkNzNjODIxZmJlNDAyZDBjYWExMWVlYTJmZjNkYjAzN2JkZjAyMzdkYjA)

## Plans
For the social and decentralizations aspects of the application, the protocol I'm looking to use and possibly expand on is the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol. For implementation of the ActivityPub protocol I'm looking into what's currently available.  Here's a brief list of links I've found:

* [r/ActivityPub](https://www.reddit.com/r/ActivityPub)
    * [ActivityPub tutorial](https://raw.githubusercontent.com/w3c/activitypub/gh-pages/activitypub-tutorial.txt)
* [A general Python ActivityPub library](https://github.com/dsblank/activitypub)

### Todo:

* Determine the best way to implement the ActivityPub protocol
  * Learn how ActivityPub works
  * Find a module for ActivityPub
* Set up registration and authentication
  * Conventional user/email and password
    * Email verification and anti-spam mechanism
  * oauth/social
* Create the features of the platform
  * Microblogging
    * Text posts
    * Image posts
    * Video Posts
    * External link posts
    * reblog posts
  * Comments and reactions
  * Feeds and following
  * Messaging
    * Anon messaging
* Create features that free the user
  * Options to backup/export/import an entire blog
  * Options to migrate a blog to a new node

## Installation

The method I'm using to install is using a docker container.

```
cd /opt
git clone https://gitlab.com/mvanorder1390/bacn.git

yum -y install docker docker-compose nginx

cat > /etc/systemd/system/bacn.service << EOF
[Unit]
Description=Bacn servoce
After=network.target docker.service
[Service]
Type=simple
WorkingDirectory=/opt/bacn
ExecStart=/bin/docker-compose -f /opt/bacn/docker-compose.yml up
ExecStop=/bin/docker-compose -f /opt/bacn/docker-compose.yml down
Restart=always
[Install]
WantedBy=multi-user.target
EOF

cat > /etc/nginx/conf.d/bacn.conf << EOF
server {
    listen 80;
    server_name bacn.social www.bacn.social;

    location /static {
      alias /opt/bacn/staticfiles;
    }

    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

}
EOF

setsebool -P httpd_can_network_connect 1

pip install pipenv
pipenv install
pipenv run python manage.py collectstatic
pipenv run python manage.py migrate
pipenv run python manage.py createsuperuser
mv db/db.sqlite3 ./

systemctl enable docker
systemctl enable bacn
systemctl enable nginx
systemctl start docker
systemctl start bacn
systemctl start nginx
```

Setting up HTTPS using letsencrypt:

```
yum -y install yum-utils
yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
yum -y install python2-certbot-nginx

certbot --nginx

sed -i 's/server_name bacn.social www.bacn.social;/server_name bacn.social;/g' /etc/nginx/conf.d/bacn.conf
cat >> /etc/nginx/conf.d/bacn.conf << EOF
server {
    listen       80;
    server_name  www.bacn.social;
    return       301 https://bacn.social\$request_uri;
}
EOF

ps -ef|grep 'nginx: master'|awk '{print $2}'|xargs kill -HUP
```

## Post installation setup

1. Log into the admin with your superuser and create an "Activity pub sites" object.


## Notes

Good informational links:

* [Fetching ActivityPub Feeds](https://gkbrk.com/2018/06/fetching-activitypub-feeds/)

## Links

[How to implement a basic ActivityPub server](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/)
