from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.translation import gettext_lazy as _


def generate_keys():
    """Generate a private/public keypair
    """
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives import serialization
    from cryptography.hazmat.primitives.asymmetric import rsa
    key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    private_key = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.BestAvailableEncryption(b"passphrase"),
    ).decode()
    public_key = key.public_key().public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ).decode()
    return private_key, public_key


class ActorManager(models.Manager):

    def create(self, *args, **kwargs):
        private_key, public_key = generate_keys()
        return super().create(*args, private_key=private_key, public_key=public_key, **kwargs)

    def get_or_create(self, *args, **kwargs):
        obj, created = super().get_or_create(*args, **kwargs)
        if created:
            obj.private_key, obj.public_key = generate_keys()
            obj.save()
        return obj, created


class ActivityPubSite(models.Model):
    site = models.OneToOneField(
        Site, null=False, related_name="activitypub", verbose_name=('ActivityPub'),
        on_delete=models.CASCADE)
    title = models.CharField(max_length=40, blank=False, verbose_name=('Title'))
    description = models.TextField(max_length=1000, blank=False, verbose_name=('Description'))
    email = models.EmailField(blank=False, verbose_name=('Contact Email'))
    contact_account = models.OneToOneField(
        User, null=False, related_name="activitypub", verbose_name=('Contact User\'s account'),
        on_delete=models.PROTECT)

    @property
    def domain(self):
        return self.site.domain

    @property
    def languages(self):
        """Array of ISO 6391 language codes the instance has chosen to advertise"""
        return settings.ACTIVITY_PUB['languages']

    @property
    def nodeinfo(self):
        return {
            "version": "2.0",  # until a new version is added, this will be hard coded
            "software": {
                "name": "Bacn",
                "Version": "0.1"
            },
            "protocols": ["activitypub"],
            "services": {
                "inbound": [],
                "outbound": []
            },
            "openRegistrations": False,
            "usage": {
                "users": User.objects.count()
            },
            "metadata": {}
        }

    @property
    def uri(self):
        return self.domain

    @property
    def urls(self):
        return {"streaming_api": "wss://{}".format(site.domain)}

    @property
    def bacn_version(self):
        return settings.ACTIVITY_PUB['bacn_version']

    def __str__(self):
        return self.title


class UserActor(models.Model):

    user = models.OneToOneField(User, null=False, related_name="actor", verbose_name=_('ActivityPub Actor'),
                                on_delete=models.CASCADE)
    public_key = models.TextField(
        max_length=2048, blank=True, verbose_name=_('Public key'))
    private_key = models.TextField(
        max_length=2048, blank=True, verbose_name=_('Private key'))

    objects = ActorManager()

    def as_json_ld(self, ap_domain):
        return {
            "@context": [
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1"
            ],
            "id": "https://{}/user/{}".format(ap_domain, self.user.username),
            "type": "Person",
            "preferredUsername": self.user.username,
            "inbox": "https://{}/user/{}/inbox".format(ap_domain, self.user.username),
            "outbox": "https://{}/user/{}/outbox".format(ap_domain, self.user.username),
            "publicKey": {
                "id": "https://{}/user/{}#main-key".format(ap_domain, self.user.username),
                "owner": "https://{}/user/{}".format(ap_domain, self.user.username),
                "publicKeyPem": "{}".format(self.main_key)
            }
        }

    @property
    def main_key(self):
        return self.public_key
