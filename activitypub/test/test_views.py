import json

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase
from django.urls import reverse
from django.test.client import Client

from ..models import ActivityPubSite


class WebfingerTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@user.com', password='top_secret')

    def test_user_with_no_social_account(self):
        pass

    def test_site_error(self):
        client = Client()
        response = client.get(reverse('activitypub:webfinger'), {
            'resource': 'acct:{}@bacn.social'.format(self.user.username)
        })
        self.assertEqual(response.status_code, 500)
        self.assertJSONEqual(json.dumps(response.data), json.dumps({
            'code': 500,
            'status': 'No activitypub has been configured for site related to {}@bacn.social'.format(self.user.username)
        }))

    def test_no_site_error(self):
        client = Client(HTTP_ACCEPT="application/ld+json profile=https://www.w3.org/ns/activitystreams")
        ActivityPubSite.objects.create(site=Site.objects.first(), contact_account=self.user)
        response = client.get(
            reverse('activitypub:activitypub.profile', args=[self.user.username]),
            HTTP_ACCEPT='application/ld+json'
        )

        self.assertEqual(response.status_code, 200)
        self.maxDiff = True
        data = {
            "@context": ["https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1"],
            "id": "https://example.com/user/{}".format(self.user.username),
            "type": "Person",
            "preferredUsername": self.user.username,
            "inbox": "https://example.com/user/{}/inbox".format(self.user.username),
            "outbox": "https://example.com/user/{}/outbox".format(self.user.username),
            "publicKey": {
                "id": "https://example.com/user/{}#main-key".format(self.user.username),
                "owner": "https://example.com/user/{}".format(self.user.username),
                "publicKeyPem": self.user.actor.public_key
            }
        }
        self.assertJSONEqual(json.dumps(response.data), json.dumps(data))


class Nodeinfo(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@user.com', password='top_secret')

    def test_node_info_fail(self):
        client = Client()
        response = client.get(
            reverse("activitypub:wellknown.nodeinfo"),
            HTTP_ACCEPT="application/ld+json"
        )
        self.assertEqual(response.status_code, 500)
        self.assertJSONEqual(json.dumps(response.data), json.dumps({
            'code': 500,
            'status': 'No activitypub has been configured for site.'
        }))

    def test_node_info(self):
        client = Client()
        ap_site = ActivityPubSite.objects.create(site=Site.objects.first(), contact_account=self.user)
        response = client.get(
            reverse("activitypub:wellknown.nodeinfo"),
            HTTP_ACCEPT="application/ld+json"
        )

        data = {
            "links": [
                {
                    "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                    "href": "https://{}{}".format(
                        ap_site.domain,
                        reverse('activitypub:nodeinfo', args=["2.0"]))
                }
            ]

        }

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(json.dumps(response.data), json.dumps(data))

        nodeinfo_link = response.data['links'][0]

        response = client.get(
            nodeinfo_link["href"],
            HTTP_ACCEPT=f"application/ld+json; profile={nodeinfo_link['rel']}"
        )

        data = {
            "version": "2.0",  # until a new version is added, this will be hard coded
            "software": {
                "name": "Bacn",
                "Version": "0.1"
            },
            "protocols": ["activitypub"],
            "services": {
                "inbound": [],
                "outbound": []
            },
            "openRegistrations": False,
            "usage": {
                "users": User.objects.count()
            },
            "metadata": {}
        }

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(json.dumps(response.data), json.dumps(data))
