from django.urls import include, path
from .views import InstanceView, NodeinfoView, ProfileView, WebfingerView, WellknownNodeinfoView

app_name = 'activitypub'
urlpatterns = [
    path('bacn_api/v1/instance', InstanceView.as_view(), name='activitypub.instance'),
    path('user/<username>', ProfileView.as_view(), name='activitypub.profile'),
    path('.well-known/webfinger', WebfingerView.as_view(), name='webfinger'),
    path('.well-known/nodeinfo', WellknownNodeinfoView.as_view(), name='wellknown.nodeinfo'),
    path('nodeinfo/<version>', NodeinfoView.as_view(), name='nodeinfo'),
]
