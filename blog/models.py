from django.db import models
from django.contrib.auth.models import User
from django.template import loader
from django.urls import reverse


class Blog(models.Model):
    name = models.TextField(max_length=50, null=False)
    owner = models.ForeignKey(User, null=False, on_delete=models.CASCADE)


class Tag(models.Model):
    text = models.TextField(max_length=120, null=False)

    def get_absolute_url(self):
        return reverse('blog:tag', args=[str(self.text)])

    @classmethod
    def from_tags_str(cls, tags_str):
        """Using a provided string of tags, get the database objects for each of those tags.

        :param tags_str: string of tags from a post form
        :type tags_str: str
        :return: list of Tag items for each tag in the string
        :rtype: list of Tag
        """

        # If tag_str is invalid or empty return None
        if '#' not in tags_str:
            return []

        # Get all unique tags from the provided string
        tag_strs_set = set(tag_str.strip() for tag_str in tags_str.split('#')[1:])

        return list(cls.objects.get_or_create(text=tag_str)[0] for tag_str in tag_strs_set if tag_str)


class Post(models.Model):
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    blog = models.ForeignKey(Blog, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    text = models.TextField(max_length=500000, null=False)
    tags = models.ManyToManyField(Tag)

    @property
    def as_template(self):
        template = loader.get_template('blog/blog_post.html')
        context = {
            "title": self.title,
            "user": self.user,
            "text": self.text,
            "tags": self.tags.all()
        }
        return template.render(context)
