from django.contrib.auth.models import User
from django.test import RequestFactory, TestCase
from django.urls import resolve, reverse
from blog.models import Post, Blog

create_post_url = reverse('blog:create_post')


class BlogTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='testuser', email='test@user.com', password='top_secret')
        self.user_blog = Blog.objects.create(name="Test blog", owner=self.user)

        self.user2 = User.objects.create_user(
            username='testuser2', email='test2@user.com', password='top_secret')
        self.user2_blog = Blog.objects.create(name="Test blog 2", owner=self.user2)

    def test_create_text_post(self):
        post_data = {
            "blog": self.user_blog.id,
            "title": "De finibus bonorum et malorum 1.10",
            "text": "32 But I must explain to you how all this mistaken idea of reprobating " +
                    "pleasure and extolling pain arose. To do so, I will give you a complete " +
                    "account of the system, and expound the actual teachings of the great " +
                    "explorer of truth, the master-builder of human happiness. No one rejects, " +
                    "dislikes or avoids pleasure itself, because it is pleasure, but because " +
                    "those who do not know how to pursue pleasure rationally encounter " +
                    "consequences that are extremely painful. Nor again is there anyone who " +
                    "loves or pursues or desires to obtain pain of itself, because it is pain, " +
                    "but because occasionally circumstances occur in which toil and pain can " +
                    "procure him some great pleasure. To take a trivial example, which of us " +
                    "ever undertakes laborious physical exercise, except to obtain some " +
                    "advantage from it? But who has any right to find fault with a man who " +
                    "chooses to enjoy a pleasure that has no annoying consequences, or one who " +
                    "avoids a pain that produces no resultant pleasure? 33 On the other hand, we " +
                    "denounce with righteous indignation and dislike men who are so beguiled and " +
                    "demoralized by the charms of the pleasure of the moment, so blinded by " +
                    "desire, that they cannot foresee the pain and trouble that are bound to " +
                    "ensue; and equal blame belongs to those who fail in their duty through " +
                    "weakness of will, which is the same as saying through shrinking from toil " +
                    "and pain. These cases are perfectly simple and easy to distinguish. In a " +
                    "free hour, when our power of choice is untrammelled and when nothing " +
                    "prevents our being able to do what we like best, every pleasure is to be " +
                    "welcomed and every pain avoided. But in certain emergencies and owing to the" +
                    " claims of duty or the obligations of business it will frequently occur that" +
                    " pleasures have to be repudiated and annoyances accepted. The wise man " +
                    "therefore always holds in these matters to this principle of selection: he " +
                    "rejects pleasures to secure other greater pleasures, or else he endures " +
                    "pains to avoid worse pains. 34 This being the theory I hold, why need I be " +
                    "afraid of not being able to reconcile it with the case of the Torquati my " +
                    "ancestors? Your references to them just now were historically correct, and " +
                    "also showed your kind and friendly feeling towards myself; but all the same " +
                    "I am not to be bribed by your flattery of my family, and you will not find " +
                    "me a less resolute opponent. Tell me, pray, what explanation do you put " +
                    "upon their actions? Do you really believe that they charged an armed enemy, " +
                    "or treated their children, their own flesh and blood, so cruelly, without a " +
                    "thought for their own interest or advantage? Why, even wild animals do not " +
                    "act in that way; they do not run amok so blindly that we cannot discern any " +
                    "purpose in their movements and their onslaughts. Can you then suppose that " +
                    "those heroic men performed their famous deeds without any motive at all? 35 " +
                    "What their motive was, I will consider later on: for the present I will " +
                    "confidently assert, that if they had a motive for those undoubtedly glorious" +
                    " exploits, that motive was not a love of virtue in and for itself. — He " +
                    "wrested the necklet from his foe. — Yes, and saved himself from death. — But" +
                    " he braved great danger. — Yes, before the eyes of an army. — What did he " +
                    "get by it? — Honour and esteem, the strongest guarantees of security in " +
                    "life. — He sentenced his own son to death. — If from no motive, I am sorry " +
                    "to be the descendant of anyone so savage and inhuman; but if his purpose was" +
                    " by inflicting pain upon himself to establish his authority as a commander, " +
                    "and to tighten the reins of discipline during a very serious war by holding " +
                    "over his army the fear of punishment, then his action aimed at ensuring the " +
                    "safety of his fellow-citizens, upon which he knew his own depended. 36 And " +
                    "this is a principle of wide application. People of your school, and " +
                    "especially yourself, who are so diligent a student of history, have found a " +
                    "favourite field for the display of your eloquence in recalling the stories " +
                    "of brave and famous men of old, and in praising their actions, not on " +
                    "utilitarian grounds, but on account of the splendour of abstract moral " +
                    "worth. But all of this falls to the ground if the principle of selection " +
                    "that I have just mentioned be established, — the principle of forgoing " +
                    "pleasures for the purpose of getting greater pleasures, and enduring pains " +
                    "for the sake of escaping greater pains.",
            "tags": "#test #lorem ipsum"
        }

        request = self.factory.post(create_post_url, data=post_data)
        request.user = self.user

        response = resolve(create_post_url).func(request)

        blog_posts = Post.objects.all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(blog_posts), 1)
        post = blog_posts[0]
        self.assertEqual(post.user, self.user)
        self.assertEqual(post.title, post_data['title'])
        self.assertEqual(post.text, post_data['text'])
        # Todo: asserting equality of the result joined tags intermittently fails due to the order not being consistent
        self.assertEqual(' '.join(('#' + tag.text for tag in post.tags.all())), post_data['tags'])

        # Test creating a post without tags
        post_data = {
            "blog": self.user_blog.id,
            "title": "Tagless post",
            "text": "This ia a blog post with no tags",
            "tags": ""
        }

        request = self.factory.post(create_post_url, data=post_data)
        request.user = self.user

        response = resolve(create_post_url).func(request)

        self.assertEqual(response.status_code, 200)
        post = Post.objects.get(title=post_data["title"])
        self.assertEqual(post.user, self.user)
        self.assertEqual(post.title, post_data['title'])
        self.assertEqual(post.text, post_data['text'])
        self.assertEqual(list(post.tags.all()), [])

    def test_create_without_login(self):
        response = self.client.get(create_post_url)

        self.assertRedirects(response, reverse('account_login') + "?next=" + create_post_url)
