import re

from django.contrib.auth.models import User
from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.views.generic.base import TemplateView
from allauth.socialaccount.models import SocialAccount

from core.models import UserProfile
from .forms import PostForm
from .models import Tag


class CreatePostView(TemplateView):

    template_name = "blog/create_post.html"

    def get_context_data(self, *args, form=None, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context['form'] = form or PostForm()
        return context

    def post(self, request, *args, **kwargs):
        """Handle post requests for the Create Post view.
        """

        # Get or create tag objects for every tag listed in the form
        tags = Tag.from_tags_str(self.request.POST['tags'])

        form = PostForm.from_post_data(
            post_data=self.request.POST,
            user_id=self.request.user.id
        )

        if form.is_valid():
            new_post = form.save()
            new_post.tags.set(tags)

        context = self.get_context_data(form=form)
        context['form_tags'] = ' '.join(['#' + t.text for t in tags])
        return render(request, template_name=self.template_name, context=context)


class BlogView(TemplateView):

    template_name = "blog/blog.html"

    # ProfileView.user is the user that is currently logged in.
    user = None

    # ProfileView.profile is the profile being viewed.  If none is specified it will be the profile
    # of the ProfileView.user
    profile = None

    def dispatch(self, request, *args, **kwargs):
        # Since rest_framework doesn't support jrd+json requests, a workaround is necessary until
        # it does
        if re.match(r'application/(activity\|ld)+json', request.META.get('HTTP_ACCEPT', '')):
            return resolve(reverse('')).func(request, *args, **kwargs)
        return super(BlogView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        """Before get_context_data is called, the profile is located. or a 404 is returned."""
        # Get an account if specified, or try to get account that's logged in
        self.user = self.request.user
        try:
            profile_user = User.objects.get(username=kwargs['username'])
        except KeyError:
            profile_user = self.user
        except User.DoesNotExist:
            return HttpResponseNotFound('<h1>Page not found</h1>')

        self.blog_posts = profile_user.post_set.all()[::-1]
        self.profile, created = UserProfile.objects.get_or_create(user=profile_user)

        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        social_account = SocialAccount.objects.get(user=self.profile.user)

        context['user'] = self.user
        context['profile'] = self.profile
        context['social'] = {
            # Todo: Fix social link
            #'link': social_account.extra_data.get('link'),
            'provider': social_account.provider
        }
        context['blog_posts'] = self.blog_posts
        return context


class TagView(TemplateView):
    pass
