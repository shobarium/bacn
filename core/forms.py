from django import forms


class SettingsForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    new_password = forms.CharField(widget=forms.PasswordInput)
    verify_password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user

    def clean_password(self):
        password = self.cleaned_data['password']

        if not self.user.check_password(password):
            raise forms.ValidationError("Invalid password")
        return password

    def clean_verify_password(self):
        new_password = self.cleaned_data['new_password']
        verify_password = self.cleaned_data['verify_password']

        if verify_password and new_password != verify_password:
            raise forms.ValidationError("Passwords don't match")
        return verify_password

    def save(self):
        new_password = self.cleaned_data['new_password']
        if new_password:
            self.user.set_password(new_password)
            self.user.save()
