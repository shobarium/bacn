from bs4 import BeautifulSoup
from django.test import Client, RequestFactory, TestCase
from django.contrib.auth.models import User
from django.urls import reverse


class UserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='testuser')
        self.user.set_password('12345')
        self.user.email = 'test@email.com'
        self.user.save()

    def test_update_user_password(self):
        client = Client()
        client.login(username='testuser', password='12345')
        settings_page = client.get(reverse('account_settings'))
        settings_soup = BeautifulSoup(settings_page.content, features="html.parser").find(id='settings-form')
        self.assertEqual(settings_page.context_data['form'].initial['email'], self.user.email)
        csrf_token = settings_soup.find('input', {'name': 'csrfmiddlewaretoken'}).get('value')
        client.post(reverse('account_settings'), {
            'csrfmiddlewaretoken': csrf_token,
            'email': self.user.email,
            'password': '12345',
            'new_password': '123456',
            'verify_password': '123456'
        })
        self.assertTrue(User.objects.get(username='testuser').check_password('123456'))

    def test_invalid_update_user_password(self):
        client = Client()
        client.login(username='testuser', password='12345')
        settings_page = client.get(reverse('account_settings'))
        settings_soup = BeautifulSoup(settings_page.content, features="html.parser").find(id='settings-form')
        self.assertEqual(settings_page.context_data['form'].initial['email'], self.user.email)
        csrf_token = settings_soup.find('input', {'name': 'csrfmiddlewaretoken'}).get('value')
        client.post(reverse('account_settings'), {
            'csrfmiddlewaretoken': csrf_token,
            'email': self.user.email,
            'password': '1234',
            'new_password': '1234567',
            'verify_password': '1234567'
        })
        self.assertTrue(User.objects.get(username='testuser').check_password('12345'))

    def test_mismatch_update_user_password(self):
        client = Client()
        client.login(username='testuser', password='12345')
        settings_page = client.get(reverse('account_settings'))
        settings_soup = BeautifulSoup(settings_page.content, features="html.parser").find(id='settings-form')
        self.assertEqual(settings_page.context_data['form'].initial['email'], self.user.email)
        csrf_token = settings_soup.find('input', {'name': 'csrfmiddlewaretoken'}).get('value')
        client.post(reverse('account_settings'), {
            'csrfmiddlewaretoken': csrf_token,
            'email': self.user.email,
            'password': '12345',
            'new_password': '1234567',
            'verify_password': '12345'
        })
        self.assertTrue(User.objects.get(username='testuser').check_password('12345'))
