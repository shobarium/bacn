from django.contrib.auth.decorators import login_required
from django.urls import include, path
from .views import HomeView, ProfileView, SearchView, SettingsView

urlpatterns = [
    path('accounts/profile/', ProfileView.as_view(), name='profile'),
    path('accounts/settings/', login_required(SettingsView.as_view()), name='account_settings'),
    path('', HomeView.as_view(), name='home'),
    path('search', SearchView.as_view(), name='search')
]
