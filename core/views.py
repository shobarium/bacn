from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic.base import TemplateView

from .forms import SettingsForm
from blog.views import BlogView
from blog.models import Post, Tag


class HomeView(TemplateView):

    template_name = "core/home.html"


class ProfileView(BlogView):

    template_name = "core/profile.html"


class SearchView(TemplateView):

    template_name = "core/search.html"

    @staticmethod
    def search(search_text, model, attributes):
        """A rudimentary search that looks for all works provided in all attributes provided of the specified model.
        """
        items = set()
        for word in search_text.split():
            for attribute in attributes:
                items |= set(model.objects.filter(**{attribute + '__contains': word}))
        return items

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search_text = self.request.GET.get('search')
        context.update({
            'people': self.search(search_text, User, ['username', 'first_name', 'last_name']),
            'posts': self.search(search_text, Post, ['title', 'text']),
            'tags': self.search(search_text, Tag, ['text', ])
        })
        return context


class SettingsView(TemplateView):

    template_name = 'core/user_settings.html'

    def get_context_data(self, form=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = form or SettingsForm(initial={
            'email': self.request.user.email,
        })
        return context

    def post(self, request, *args, **kwargs):
        form = SettingsForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
        context = self.get_context_data(form=form)
        return render(request, template_name=self.template_name, context=context)
