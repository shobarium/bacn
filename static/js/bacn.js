"use strict";

function ajax(url, callback, post_data) {
  let xhttp = new XMLHttpRequest();

  if (post_data === undefined) {
    xhttp.open("GET", url, true);
  } else {
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
  }
  xhttp.onreadystatechange = callback;
  xhttp.send(post_data);
}

class Bacn {
  constructor () {
    let function_links = document.querySelectorAll('.func-link');
    for(let link of function_links) {
      link.addEventListener("click", this[link.dataset.func])
    }
  };

  not_implemented (Event) {
    alert('Not implemented');
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
  window.bacn = new Bacn();
});
